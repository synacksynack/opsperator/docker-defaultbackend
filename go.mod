module k8s.io/ingress-nginx

go 1.15

require (
	github.com/prometheus/client_golang v1.7.1
	github.com/prometheus/client_model v0.2.0
	github.com/prometheus/common v0.14.0
)
