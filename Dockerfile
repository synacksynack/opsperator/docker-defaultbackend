FROM docker.io/golang:1.15 AS builder

# Nginx Ingress default-backend image for Kubernetes

WORKDIR /go/src/github.com/faust64/nginx-ingress-controller-defaultbackend

COPY go.* *.go ./

RUN set -x \
    && if test `uname -m` = aarch64; then \
	export GOARCH=arm64; \
    elif test `uname -m` = armv7l; then \
	export GOARCH=arm; \
    else \
	export GOARCH=amd64; \
    fi \
    && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo \
	-ldflags "-s -w" \
	-o ./custom-error-pages


FROM nginx:1.21-alpine

LABEL io.k8s.description="Nginx Default Backend Image." \
      io.k8s.display-name="Nginx Default Backend" \
      io.openshift.expose-services="8080:http,8081:http" \
      io.openshift.tags="nginx,default-backend" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-defaultbackend" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.0.0"

COPY --from=builder /go/src/github.com/faust64/nginx-ingress-controller-defaultbackend/custom-error-pages /bin/custom-error-pages
COPY /config /

RUN set -x \
    && mkdir -p /var/cache/nginx /var/run \
    && chown -R 1000:0 /www /etc/nginx /var/cache/nginx /var/run \
    && chmod -R g=u /www /etc/nginx /var/cache/nginx /var/run

ENTRYPOINT ["/run-defaultbackend.sh"]
USER 1000
