# nginx-ingress-controller-defaultbackend

Based on https://github.com/kubernetes/ingress-nginx/tree/master/images/custom-error-pages

Build with:

```sh
$ make build
```

Test with:

```sh
$ make test
```

Setting it up, we would first deploy the default-backend service, in
any namespace -- current sample installs it in `ingress-nginx`.

Serving images alongside our error pages, we would also setup an Ingress.
Pick a FQDN that would route to your Ingress Controllers, and set it into
`deployment/kubernetes/ingress.yaml` & `deployment/kubernetes/deployment.yaml`:

```sh
$ vi deploy/kubernetes/deployment.yaml
$ vi deploy/kubernetes/ingress.yaml
$ kubectl apply -n ingress-nginx deploy/kubernetes/deployment.yaml
$ kubectl apply -n ingress-nginx deploy/kubernetes/service.yaml
$ kubectl apply -n ingress-nginx deploy/kubernetes/ingress.yaml
```

Next, edit your Ingress Controller configuration:

```sh
$ kubectl edit -n ingress-nginx ds ingress-nginx-controller
```

Add the `default-backend-service` option to your controllers arguments,
pointing it to the Service we just created:

```yaml
      containers:
      - args:
        - /nginx-ingress-controller
        - --configmap=$(POD_NAMESPACE)/ingress-nginx
        - --tcp-services-configmap=$(POD_NAMESPACE)/tcp-services
        - --udp-services-configmap=$(POD_NAMESPACE)/udp-services
        - --annotations-prefix=nginx.ingress.kubernetes.io
        - --default-backend-service=$(POD_NAMESPACE)/default-backend
        - --report-node-internal-ip-address
```

Next, we would edit the Ingress Controller ConfigMap:

```sh
$ kubectl edit -n ingress-nginx cm ingress-nginx
```

Add the `custom-http-errors` key to that ConfigMap. Set the comma-separated
list of HTTP codes you want to intercept:

```yaml
apiVersion: v1
data:
  custom-http-errors: 502,503
  map-hash-bucket-size: "512"
```


Environment variables
----------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name              |    Description                | Default                                                                                   |
| :---------------------------- | ----------------------------- | ----------------------------------------------------------------------------------------- |
|  `CONTACT_ADDRESS`            | Errors Contact Address        | undef, set it to an email or support URL for users to report errors                       |
|  `DO_NGINX`                   | Nginx Proxy Toggle            | undef, starts default-backend                                                             |
|  `HIDE_WEBSERVER_NAME`        | Hides Webserver Name          | undef, webserver name is shown                                                            |
|  `LOGO_IMAGE`                 | Logo Image Asset              | `kubelemon.png`, suffix to a logo file in `www/images`                                    |
|  `PREFIX`                     | Default Backend Assets Prefix | undef, assumes there's no asset, or they would be available querying original HOST header |
|  `WALL_IMAGE`                 | Wallpaper Image Asset         | `dark.jpg`, suffix to a wallpaper file in `www/images`                                    |
|  `WEBSERVER_NAME`             | Webserver Name in Error Pages | `Nginx`                                                                                   |
