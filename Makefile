SKIP_SQUASH?=1
IMAGE=opsperator/defaultbackend:latest

.PHONY: build
build:
	@@SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: test
test:
	@@docker rm -f back || echo mkay
	@@docker rm -f front || echo mkay
	@@docker run -p 8080:8080 --name back -d $(IMAGE)
	@@MAINDEV=`ip r | awk '/default/{print $$0;exit;}' | sed 's|.* dev \([^ ]*\).*|\1|'`; \
	MAINIP=`ip r | awk "/ dev $$MAINDEV .* src /" | sed 's|.* src \([^ ]*\).*$$|\1|'`; \
	docker run -p 8081:8081 -e DO_NGINX=yay -e CE_BACK=$$MAINIP --name front -d $(IMAGE)
