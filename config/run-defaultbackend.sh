#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
if test "$DO_NGINX"; then
    LOGO_IMAGE=${LOGO_IMAGE:-kubelemon.png}
    WALL_IMAGE=${WALL_IMAGE:-dark.jpg}
    CE_BACK=${CE_BACK:-127.0.0.1}
    if ! test -s "/var/www/images/logo_$LOGO_IMAGE"; then
	LOGO_IMAGE=kubelemon.png
    fi
    if ! test -s "/var/www/images/wall_$WALL_IMAGE"; then
	WALL_IMAGE=dark.jpg
    fi
    sed -i "s|CEBACK|$CE_BACK|" /etc/nginx/nginx.conf
    ln -sf "/www/images/logo_$LOGO_IMAGE" /www/images/logo.png
    ln -sf "/www/images/wall_$WALL_IMAGE" /www/images/wall.jpg
    echo Starting nginx...
    exec nginx -g "daemon off;"
else
    PREFIX=${PREFIX:-}
    if test -z "$HIDE_WEBSERVER_NAME"; then
	WEBSERVER_NAME=${WEBSERVER_NAME:-Nginx}
    else
	WEBSERVER_NAME=
    fi
    if test -z "$CONTACT_ADDRESS"; then
	FATALMSG="If the problem persists, please contact site administrator"
	WARNINGMSG="If you thing this is a mistake, please contact site administrator"
    else
	FATALMSG="If the problem persists, please contact $CONTACT_ADDRESS"
	WARNINGMSG="If you think this is a mistake, please contact $CONTACT_ADDRESS"
    fi
    ls /www/*.htm | while read f
	do
	    sed -i -e "s|PREFIX|$PREFIX|" \
		-e "s|FATALMSG|$FATALMSG|" \
		-e "s|WARNINGMSG|$WARNINGMSG|" \
		-e "s|WEBSERVERNAME|$WEBSERVER_NAME|" \
		"$f"
	    ln -sf "$f" "${f}l"
	done
    echo Starting custom error pages process...
    exec /bin/custom-error-pages
fi
